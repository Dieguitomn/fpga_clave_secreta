----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 15.12.2020 11:05:58
-- Design Name: 
-- Module Name: clave_secreta - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clave_secreta is
    generic(
	   width : positive :=4
    );
    
    port (
        CLR_N    			: in  std_logic;  -- Asynchronous negated reset
        CLK      			: in  std_logic;  -- Positive clock
        START    			: in  std_logic;  
        RESET   		 	: in  std_logic;  -- Apaga la m�quina
        BUTTON   			: in  std_logic_vector(width -1 downto 0); --Botones placa
        DISPLAY             : out std_logic_vector(6 downto 0);    --displays
        LED_CLAVE           : out  std_logic_vector(width -1 downto 0); --4 LEDS para marcar los que llevamos
        LED_INICIO			: out  std_logic; -- led para saber que esta desde el comienzo
        LED_ACIERTO		    : out  std_logic;  -- led para saber que se ha acertado
        
        digcntrl: out std_logic_vector(7 downto 0)
    );
end clave_secreta;

architecture Behavioral of clave_secreta is

    type estate_t is (SN, S0, S1, S2, S3, S4, S5 ,S6, S7, SA); --SN estado apagado, SA acierto de contrase�a
    signal estate, nxt_estate : estate_t;
    signal digsel: std_logic_vector( 7 downto 0):="11111110";
    
begin

    estate_register: process (CLK, CLR_N)
    begin
        if RESET = '1' then
            estate <= SN;
        elsif rising_edge(CLK) then
            estate <= nxt_estate;
        end if;
    end process;
  
    --DECODIFICADOR SIGUIENTE ESTADO
    nxtstate_decod: process(estate,START, BUTTON, RESET)
    begin
        nxt_estate <= estate;
        case estate is
            --OK
            when SN =>
      	         if RESET = '1' then
                    nxt_estate <= SN;
 		         elsif START = '1' then
                    nxt_estate <= S0;       
                end if;  
            --OK
            when S0 =>
                if RESET = '1' then
                    nxt_estate <= SN;
                elsif BUTTON(0) = '1' then
                    nxt_estate <= S1;             
                end if;
           --NO OK            
            when S1 =>
                if RESET = '1' then
                    nxt_estate <= SN;
                elsif BUTTON(1) = '1' then
                    nxt_estate <= S2;         
                elsif BUTTON(0) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(2) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(3) = '1' then
                    nxt_estate <= S0;
                end if;
                        
            when S2 =>
                if RESET = '1' then
                    nxt_estate <= SN;
                elsif BUTTON(2) = '1'   then
                    nxt_estate <= S3;             
                elsif BUTTON(0) = '1' then
                    nxt_estate <= S0;       
                elsif BUTTON(3) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(1) = '1' then
                    nxt_estate <= S0;
                end if;
                              
            when S3 =>
                if RESET = '1' then
                    nxt_estate <= SN;
                elsif BUTTON(3) = '1'  then
                    nxt_estate <= S4;             
                elsif BUTTON(0) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(1) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(2) = '1' then
                    nxt_estate <= S0;    
                end if;
             
             when S4 =>
                if RESET = '1' then
                    nxt_estate <= SN;
                elsif BUTTON(0) = '1'  then
                    nxt_estate <= S5;             
                elsif BUTTON(2) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(1) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(3) = '1' then
                    nxt_estate <= S0;    
                end if;
                
              when S5 =>
                if RESET = '1' then
                    nxt_estate <= SN;
                elsif BUTTON(3) = '1'  then
                    nxt_estate <= S6;             
                elsif BUTTON(0) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(1) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(2) = '1' then
                    nxt_estate <= S0;    
                end if;
              when S6 =>
                if RESET = '1' then
                    nxt_estate <= SN;
                elsif BUTTON(1) = '1'  then
                    nxt_estate <= S7;             
                elsif BUTTON(3) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(2) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(0) = '1' then
                    nxt_estate <= S0;    
                end if;  
               
               when S7 =>
                if RESET = '1' then
                    nxt_estate <= SN;
                elsif BUTTON(2) = '1'  then
                    nxt_estate <= SA;             
                elsif BUTTON(0) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(1) = '1' then
                    nxt_estate <= S0;
                elsif BUTTON(3) = '1' then
                    nxt_estate <= S0;    
                end if;         
            when SA =>
                if RESET = '1' then
                    nxt_estate <= SN;
                elsif START = '1' then
                    nxt_estate <= S0;
                end if;
             
            when others =>
                nxt_estate <= SN;
            
        end case;
    end process;
  
  --DECODIFICADOR DE LAS SALIDAS
  output_decod: process(estate)
  begin
    case estate is    
        when SN =>
      	     LED_CLAVE	 <= "0000";
   	 	     LED_INICIO	 <= '0';
    	     LED_ACIERTO <= '0';
    	     DISPLAY     <= "1111110";
        when S0 =>
		      LED_CLAVE	  <= "0000";
   	 	      LED_INICIO  <= '1';
    	      LED_ACIERTO <= '0';
    	      DISPLAY     <= "0000001";
    	              
        when S1 =>
		      LED_CLAVE	  <= "0001";
              LED_INICIO  <= '0';
              LED_ACIERTO <= '0';
              DISPLAY     <="1001111";
        
        when S2 =>
            LED_CLAVE	<= "0011";
            LED_INICIO	<= '0';
    	    LED_ACIERTO	<= '0';
    	   DISPLAY     <= "0010010";
        
        when S3 =>
      	    LED_CLAVE	<= "0111";
            LED_INICIO	<= '0';
    	    LED_ACIERTO	<= '0';
    	    DISPLAY     <= "0000110";
        when S4 =>
      	    LED_CLAVE	<= "1111";
            LED_INICIO	<= '0';
    	    LED_ACIERTO	<= '0';
    	    DISPLAY     <= "1001100";
    	when S5 =>
      	    LED_CLAVE	<= "0111";
            LED_INICIO	<= '0';
    	    LED_ACIERTO	<= '0';
    	    DISPLAY     <= "0100100";
    	when S6 =>
      	    LED_CLAVE	<= "0011";
            LED_INICIO	<= '0';
    	    LED_ACIERTO	<= '0';
    	    DISPLAY     <= "0100000";
    	when S7 =>
      	    LED_CLAVE	<= "0001";
            LED_INICIO	<= '0';
    	    LED_ACIERTO	<= '0';
    	    DISPLAY     <= "0001111";
        when SA =>
      	    LED_CLAVE	<= "0000";
   	 	    LED_INICIO	<= '0';
    	    LED_ACIERTO	<= '1';
    	    DISPLAY     <= "0000000";
    	    
        
        when others =>
            LED_CLAVE	<= "0000";
   	 	    LED_INICIO  <= '0';
    	    LED_ACIERTO	<= '0';
            DISPLAY     <= "1111110";
    end case;
            digcntrl<=digsel;
  end process;
  
end Behavioral;