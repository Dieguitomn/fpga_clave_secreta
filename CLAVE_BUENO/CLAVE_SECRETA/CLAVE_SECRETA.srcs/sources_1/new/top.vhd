----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.12.2020 20:42:19
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    generic(
        WIDTH : positive := 4
    );
    port(
        BUTTON      : in std_logic_vector (0 to width -1); --boton pulsado
        CLK         : in std_logic;                        --se�al reloj
        CLR_N       : in std_logic;                        --reset as�ncrono
        START_FMS   : in std_logic;                        --encendido
        RESET_FMS   : in std_logic;                        --apagado
        DISPLAY     : out std_logic_vector(6 downto 0);    --displays
        LED_CLAVE   : out std_logic_vector(0 to width -1); --marcan digitos acertados
        LED_ACIERTO : out std_logic;                       --d�gito acertado
        LED_INICIO  : out std_logic;                        --inicio juego 
        
        digcntrl: out std_logic_vector(7 downto 0)
    );
end top;

architecture Behavioral of top is

    --SINCRONIZADOR
    component sincronizador
        port(
            CLK      : in STD_LOGIC; --se�al reloj global
            ASYNC_IN : in STD_LOGIC; --pulsaci�n bot�n no sinc
            SYNC_OUT : out STD_LOGIC --pulsaci�n bot�n sinc
        );
    end component;
    
    --EDGE DETECTOR
    component detector_flanco
        port(
            CLK     : in STD_LOGIC;     --se�al reloj global
            SYNC_IN : in STD_LOGIC;     --pulsaci�n bot�n sinc
            EDGE    : out STD_LOGIC     --flanco positivo ok
        );
    end component;
    
    --FMS CLAVE SECRETA CON BOTONES
    component clave_secreta
        port(
            CLR_N    			: in  std_logic;                        -- Asynchronous negated reset
            CLK      			: in  std_logic;                        -- Positive clock
            START    			: in  std_logic;                        -- Enciende m�quina
            RESET   		 	: in  std_logic;                        -- Apaga la m�quina
            BUTTON   			: in  std_logic_vector(width -1 downto 0);  -- Botones placa
            LED_CLAVE           : out  std_logic_vector(width -1 downto 0); -- 4 LEDS para marcar los que llevamos
            LED_INICIO			: out  std_logic;                       -- led para saber que esta desde el comienzo
            LED_ACIERTO		    : out  std_logic;                        -- led para saber que se ha acertado
            digcntrl            : out std_logic_vector(7 downto 0);
            DISPLAY             : out std_logic_vector(6 downto 0)
            
        );
    end component;
    
    --AUXILIAR SIGNALS
    signal sincronizada : std_logic_vector(width -1 downto 0); --salida sincronizador = entrada edge detector
    signal flanco       : std_logic_vector(width -1 downto 0); --salida edge detector = entrada fms
    signal inicio, inicio_flanco       : std_logic;
    
begin
    
    --CONEX SINCRONIZADOR BOTON 0
    inst_sincronizador_0 : sincronizador 
        port map(
            CLK      => CLK,
            ASYNC_IN => BUTTON(0),
            SYNC_OUT => sincronizada(0)
        );

    --CONEX EDGE DETECTOR BOTON 0
    inst_edge_detector_0 : detector_flanco
        port map(
            CLK     => CLK,
            SYNC_IN => sincronizada(0),
            EDGE    => flanco(0)
        ); 
        

    --CONEX SINCRONIZADOR BOTON 1
    inst_sincronizador_1 : sincronizador 
        port map(
            CLK      => CLK,
            ASYNC_IN => BUTTON(1),
            SYNC_OUT => sincronizada(1)
        );

    --CONEX EDGE DETECTOR BOTON 1
    inst_edge_detector_1 : detector_flanco
        port map(
            CLK     => CLK,
            SYNC_IN => sincronizada(1),
            EDGE    => flanco(1)
        ); 
        
        
    --CONEX SINCRONIZADOR BOTON 2
    inst_sincronizador_2 : sincronizador 
        port map(
            CLK      => CLK,
            ASYNC_IN => BUTTON(2),
            SYNC_OUT => sincronizada(2)
        );

    --CONEX EDGE DETECTOR BOTON 2
    inst_edge_detector_2 : detector_flanco
        port map(
            CLK     => CLK,
            SYNC_IN => sincronizada(2),
            EDGE    => flanco(2)
        ); 
        
    --CONEX SINCRONIZADOR BOTON 3
    inst_sincronizador_3 : sincronizador 
        port map(
            CLK      => CLK,
            ASYNC_IN => BUTTON(3),
            SYNC_OUT => sincronizada(3)
        );

    --CONEX EDGE DETECTOR BOTON 3
    inst_edge_detector_3 : detector_flanco
        port map(
            CLK     => CLK,
            SYNC_IN => sincronizada(3),
            EDGE    => flanco(3)
        ); 
        
    --CONEX SINCRONIZADOR BOTON INICIIO
    inst_sincronizador_start : sincronizador 
        port map(
            CLK      => CLK,
            ASYNC_IN => START_FMS,
            SYNC_OUT => inicio
        );

    --CONEX EDGE DETECTOR BOTON INICIO
    inst_edge_detector_start : detector_flanco
        port map(
            CLK     => CLK,
            SYNC_IN => inicio,
            EDGE    => inicio_flanco
        ); 
        
        
    --CONEX FMS
    inst_clave_secreta : clave_secreta
        --generic map(
        --   width => WIDTH
        --);
        port map(
            CLR_N    	=> CLR_N,	
            CLK      	=> CLK,
            START    	=> inicio_flanco,
            RESET   	=> RESET_FMS,
            BUTTON   	=> flanco,
            LED_CLAVE   => LED_CLAVE,     
            LED_INICIO	=> LED_INICIO,
            LED_ACIERTO	=> LED_ACIERTO,
            digcntrl 	=> digcntrl,
            DISPLAY => DISPLAY
        );
        
        
        
        
        
end Behavioral;


