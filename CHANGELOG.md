# Changelog trabajo VHDL #

### VERSIONES ###
* v1.1 (13/12/2020)

###### Add:
####### Comprobado funcionamiento del testbench de clave secreta
####### Incluido diseño del sincronizador y detector de flancos
####### Implementada la entidad top

###### Revisar:
####### Diseño de la entidad top : ¿buses de 4 bits?, ¿generics?

-----------------------------------------------------------------
* v1.2 (15/12/2020)

###### Add:
####### Se han añadido los detectores y sincronizadores necesarios
####### Se ha comprobado el correcto funcionamiento en la placa

###### Revisar:
####### Nuevas funcionalidades